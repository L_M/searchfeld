library(tidyverse)
library(qdapDictionaries)
library(gsubfn)

data("contractions")
scr <- read.csv("~/Downloads/scripts-csv(1).csv")

contractions <- contractions %>% mutate(raw=stringr::str_replace(contraction, "'", ""))

contr <- as.list(contractions$contraction)
raw <- contractions$raw
contr <- setNames(contr, raw)

scr <- scr %>% mutate( dialogue=gsubfn("(\\w+)", contr, dialogue, perl = TRUE) )

ccontr <- as.list(str_to_title(contractions$contraction))
craw <- str_to_title(contractions$raw)
ccontr <- setNames(ccontr, craw)

scr <- scr %>% mutate( dialogue=gsubfn("(\\w+)", ccontr, dialogue, perl = TRUE) )

ucontr <- as.list(str_to_upper(contractions$contraction))
uraw <- str_to_upper(contractions$raw)
ucontr <- setNames(ucontr, uraw)

scr <- scr %>% mutate( dialogue=gsubfn("(\\w+)", ucontr, dialogue, perl = TRUE) )

write_csv(scr, 'scr.csv')
